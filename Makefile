CC=gcc $(DEBUG_ARGS) -g -std=c99 -Werror -Wall -Wextra -pedantic-errors -pedantic \
 -Wshadow \
 -Wunused-parameter \
 -Wmissing-field-initializers \
 -Wmissing-noreturn \
 -Wmissing-declarations \
 -Wmissing-prototypes \
 -Wmissing-format-attribute \
 -Wpointer-arith \
 -Wwrite-strings \
 -Wformat \
 -Wformat-nonliteral \
 -Wformat-security \
 -Wswitch-default \
 -Winit-self \
 -Wundef \
 -Waggregate-return \
 -Wnested-externs \
 -Wno-unused-function

.PHONY: all
all: change-passwords

change-passwords: src/change-passwords.c
	$(CC) -o $@ $< `pkg-config --libs --cflags libsecret-1`

.PHONY: clean
clean:
	rm -rf src/*.o

.PHONY: leaks
leaks: change-passwords
	G_DEBUG=gc-friendly G_SLICE=always-malloc valgrind --tool=memcheck --leak-check=full --show-reachable=yes --trace-children=yes --suppressions=.valgrind ./change-passwords --quit
