#define _XOPEN_SOURCE 700
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>

#include <libsecret/secret.h>

extern const SecretSchema *SECRET_SCHEMA_NOTE;

typedef struct CliArgs {
    gboolean all_attributes;
    gboolean change_password;
    gchar *match_application;
    gchar *match_schema;
    gchar *match_label;
    gchar *match_username;
    gchar *match_secret;
} CliArgs;

typedef struct Passwords {
    gchar *old_password;
    gchar *new_password;
} Passwords;


static CliArgs ARGS;
static Passwords PASSWORDS;


static void print_attribute(GHashTable *attributes, const gchar *name) {
    gchar *value = g_hash_table_lookup(attributes, name);
    if (value) {
        g_print("  %s: %s\n", name, value);
    }
}


static void process_secret_item(SecretItem *secret_item) {
    GHashTable *attributes = secret_item_get_attributes(secret_item);

    if (ARGS.match_application) {
        gchar *application = g_hash_table_lookup(attributes, "application");
        gchar *found = application ? g_strstr_len(application, -1, ARGS.match_application) : NULL;
        if (!found) {
            g_hash_table_unref(attributes);
            return;
        }
    }

    gchar *schema = secret_item_get_schema_name(secret_item);
    if (ARGS.match_schema) {
        gchar *found = schema ? g_strstr_len(schema, -1, ARGS.match_schema) : NULL;
        if (!found) {
            g_hash_table_unref(attributes);
            g_free(schema);
            return;
        }
    }

    gchar *label = secret_item_get_label(secret_item);
    if (ARGS.match_label) {
        gchar *found = label ? g_strstr_len(label, -1, ARGS.match_label) : NULL;
        if (!found) {
            g_hash_table_unref(attributes);
            g_free(schema);
            g_free(label);
            return;
        }
    }

    gchar *username = g_hash_table_lookup(attributes, "username_value");
    if (ARGS.match_username) {
        gchar *found = username ? g_strstr_len(username, -1, ARGS.match_username) : NULL;
        if (!found) {
            g_hash_table_unref(attributes);
            g_free(schema);
            g_free(label);
            return;
        }
    }

    GError *error = NULL;
    secret_item_load_secret_sync(secret_item, NULL, &error);
    if (error) {
        g_printerr("Error getting secret: %s\n", error->message);
        g_error_free(error);
        g_hash_table_unref(attributes);
        g_free(schema);
        g_free(label);
        return;
    }

    SecretValue *secret_value = secret_item_get_secret(secret_item);
    const gchar *secret = secret_value_get_text(secret_value);
    if (PASSWORDS.old_password && g_strcmp0(secret, PASSWORDS.old_password)) {
        g_free(schema);
        g_free(label);
        secret_value_unref(secret_value);
        return;
    }
    else if (ARGS.match_secret) {
        if (!g_strstr_len(secret, -1, ARGS.match_secret)) {
            g_free(schema);
            g_free(label);
            secret_value_unref(secret_value);
            return;
        }
    }

    // Change the secret
    gchar *secret_copy = g_strdup(secret);
    secret_value_unref(secret_value);
    if (ARGS.change_password) {
        SecretValue *new_secret_value = secret_value_new(PASSWORDS.new_password, -1, "text/plain");
        error = NULL;
        g_print("Changing password to %s\n", PASSWORDS.new_password);
        secret_item_set_secret_sync(secret_item, new_secret_value, NULL, &error);
        if (error) {
            g_printerr("Error changin secret: %s\n", error->message);
            g_error_free(error);
        }
        secret_value_unref(new_secret_value);
    }

    g_print("  label: %s\n", label);
    g_print("  schema: %s\n", schema);
    g_print("  username: %s\n", username);
    g_print("  secret: %s\n", secret_copy);
    g_free(schema);
    g_free(label);
    g_free(secret_copy);

    if (!ARGS.all_attributes) {
        print_attribute(attributes, "signon_realm");
        print_attribute(attributes, "application");
        print_attribute(attributes, "xdg:schema");
        g_print("\n");
        g_hash_table_unref(attributes);
        return;
    }

    GHashTableIter attributes_iter;
    gpointer key;
    gpointer value;
    g_hash_table_iter_init(&attributes_iter, attributes);
    while (g_hash_table_iter_next(&attributes_iter, &key, &value)) {
        gchar *attribute_name = (gchar *) key;
        gchar *attribute_value = (gchar *) value;
        g_print("  > %s: %s\n", attribute_name, attribute_value);
    }
    g_print("\n");
    g_hash_table_unref(attributes);
}


static void process_collection(SecretCollection *collection) {
    gchar *collection_label = secret_collection_get_label(collection);
    int is_login_cmp = g_strcmp0(collection_label, "Login");
    g_free(collection_label);

    if (is_login_cmp) {
        return;
    }

    GList *secret_items = secret_collection_get_items(collection);
    for (GList *iter_item = secret_items; iter_item != NULL; iter_item = iter_item->next) {
        SecretItem *secret_item = iter_item->data;
        process_secret_item(secret_item);
    }
    g_list_free_full(secret_items, g_object_unref);
}


static gboolean parse_args(int argc, char *argv[]) {
    ARGS.all_attributes    = FALSE;
    ARGS.change_password   = FALSE;
    ARGS.match_application = NULL;
    ARGS.match_schema      = NULL;
    ARGS.match_label       = NULL;
    ARGS.match_username    = NULL;
    ARGS.match_secret      = NULL;

    GOptionEntry entries[] = {
        { "dump",        'd', 0, G_OPTION_ARG_NONE,   &ARGS.all_attributes,    "Dump all attributes",     NULL },
        { "application", 'a', 0, G_OPTION_ARG_STRING, &ARGS.match_application, "Match application",       NULL },
        { "schema",      's', 0, G_OPTION_ARG_STRING, &ARGS.match_schema,      "Match schema",            NULL },
        { "label",       'l', 0, G_OPTION_ARG_STRING, &ARGS.match_label,       "Match label",             NULL },
        { "username",    'u', 0, G_OPTION_ARG_STRING, &ARGS.match_username,    "Match username",          NULL },
        { "secret",      'p', 0, G_OPTION_ARG_STRING, &ARGS.match_secret,      "Match secret (password)", NULL },
        { "change",      'c', 0, G_OPTION_ARG_NONE,   &ARGS.change_password,   "Change password",         NULL },
        { NULL }
    };
    GError *error = NULL;
    GOptionContext *context = g_option_context_new("- change passwords in keystore");
    g_option_context_add_main_entries(context, entries, NULL);
    gboolean parsed = g_option_context_parse(context, &argc, &argv, &error);

    if (!parsed) {
        g_print("Option parsing failed: %s\n", error->message);
        g_error_free(error);
        g_option_context_free(context);
        return FALSE;
    }

    ARGS.match_application = g_strdup(ARGS.match_application);
    ARGS.match_schema      = g_strdup(ARGS.match_schema);
    ARGS.match_label       = g_strdup(ARGS.match_label);
    ARGS.match_username    = g_strdup(ARGS.match_username);
    ARGS.match_secret      = g_strdup(ARGS.match_secret);

    g_option_context_free(context);
    return TRUE;
}


static char* read_password(gchar const *prompt) {
    int stream_fileno = fileno(stdin);

    // Turn echoing off and fail if we can't.
    struct termios termios_old;
    if (tcgetattr(stream_fileno, &termios_old) != 0) {
        return NULL;
    }

    struct termios termios_new = termios_old;
    termios_new.c_lflag &= ~ECHO;
    if (tcsetattr(stream_fileno, TCSAFLUSH, &termios_new) != 0) {
        return NULL;
    }

    // Read the password.
    g_print("%s", prompt);
    char *password = NULL;
    size_t password_length = 0;
    ssize_t read = getline(&password, &password_length, stdin);
    g_print("\n");
    if (read == -1) {
        if (password) {
            free(password);
        }
        return NULL;
    }

    // Replace the enter with a null terminated string
    password[read - 1] = '\0';

    // Restore terminal.
    (void) tcsetattr(stream_fileno, TCSAFLUSH, &termios_old);

    return password;
}


static gchar* get_password(gchar const *prompt, gboolean ask_once) {
    gchar *initial_prompt = g_strdup_printf("%s password: ", prompt);
    char *password = read_password(initial_prompt);
    g_free(initial_prompt);
    if (!password) {
        g_print("Failed to read password\n");
        return NULL;
    }

    if (ask_once) {
        return (gchar *) password;
    }

    gchar *confirmation_prompt = g_strdup_printf("%s password (confirmation): ", prompt);
    char *password_confirm = read_password(confirmation_prompt);
    g_free(confirmation_prompt);
    if (!password_confirm) {
        g_print("Failed to read password confirmation\n");
        free(password);
        return NULL;
    }

    if (g_strcmp0(password, password_confirm)) {
        g_print("%s password and password confirmation mismatch\n", prompt);
        free(password);
        free(password_confirm);
        return NULL;
    }
    free(password_confirm);

    return (gchar *) password;
}


int main (int argc, char *argv[]) {
    if (!parse_args(argc, argv)) {
        return EXIT_FAILURE;
    }


    // Read the passwords
    PASSWORDS.old_password = NULL;
    PASSWORDS.new_password = NULL;
    if (ARGS.change_password) {
        PASSWORDS.old_password = get_password("Old", TRUE);
        if (!PASSWORDS.old_password) {
            return EXIT_FAILURE;
        }

        PASSWORDS.new_password = get_password("New", FALSE);
        if (!PASSWORDS.new_password) {
            g_free(PASSWORDS.old_password);
            return EXIT_FAILURE;
        }
    }


    // Connect to the keyring
    GError *error = NULL;
    SecretService *secret_service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, NULL, &error);
    if (error) {
        g_printerr("Error connecting to Secret Service: %s\n", error->message);
        g_error_free(error);

        g_free(PASSWORDS.old_password);
        g_free(PASSWORDS.new_password);

        if (secret_service) {
            g_object_unref(secret_service);
        }

        g_free(ARGS.match_application);
        g_free(ARGS.match_schema);
        g_free(ARGS.match_label);
        g_free(ARGS.match_username);
        g_free(ARGS.match_secret);

        return EXIT_FAILURE;
    }


    // Scan the secrets
    GList *collections = secret_service_get_collections(secret_service);
    for (GList *iter_collection = collections; iter_collection != NULL; iter_collection = iter_collection->next) {
        SecretCollection *collection = (SecretCollection *) iter_collection->data;
        process_collection(collection);
    }
    g_list_free_full(collections, g_object_unref);


    // Cleanup
    g_free(PASSWORDS.old_password);
    g_free(PASSWORDS.new_password);

    if (secret_service) {
        g_object_unref(secret_service);
    }

    g_free(ARGS.match_application);
    g_free(ARGS.match_schema);
    g_free(ARGS.match_label);
    g_free(ARGS.match_username);
    g_free(ARGS.match_secret);

    return EXIT_SUCCESS;
}

